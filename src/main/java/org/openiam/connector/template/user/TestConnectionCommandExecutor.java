package org.openiam.connector.template.user;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openiam.api.connector.model.UserConnectorObject;
import org.openiam.api.connector.user.response.TestProvisioningConnectorResponse;
import org.openiam.connector.core.base.exception.ConnectorException;
import org.openiam.connector.template.base.AbstractTemplateConnectorCommand;
import org.springframework.stereotype.Component;

/**
 *
 * Test connection to the target system.
 * Called once a minute and serves to display the status of the system on Managed System page.
 * @author anton.novikov@openiam.com
 *
 */
@Component("testConnectionCommand")
public class TestConnectionCommandExecutor extends AbstractTemplateConnectorCommand<UserConnectorObject, TestProvisioningConnectorResponse> {
    private static final Log log = LogFactory.getLog(TestConnectionCommandExecutor.class);

    @Override
    public TestProvisioningConnectorResponse perform(UserConnectorObject connectorObject) throws ConnectorException {
        TestProvisioningConnectorResponse response = new TestProvisioningConnectorResponse();
        //TODO
        return response;
    }
}
