package org.openiam.connector.template.user;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openiam.api.connector.model.UserConnectorObject;
import org.openiam.api.connector.user.response.DeleteUserProvisioningConnectorResponse;
import org.openiam.base.ws.ResponseStatus;
import org.openiam.connector.core.base.exception.ConnectorException;
import org.openiam.connector.template.base.AbstractTemplateConnectorCommand;
import org.springframework.stereotype.Component;

/**
 *
 * Delete user from target system (the possibility of this operation depends on the target system).
 * may be called during user removal from OpenIAM.
 *
 */
@Component("deleteUserCommand")
public class DeleteUserCommandExecutor extends AbstractTemplateConnectorCommand<UserConnectorObject, DeleteUserProvisioningConnectorResponse> {
    private static final Log log = LogFactory.getLog(DeleteUserCommandExecutor.class);

    @Override
    public DeleteUserProvisioningConnectorResponse perform(UserConnectorObject connectorObject) throws ConnectorException {
        DeleteUserProvisioningConnectorResponse response = new DeleteUserProvisioningConnectorResponse();
        //TODO
        response.setStatus(ResponseStatus.SUCCESS);
        return response;
    }
}
