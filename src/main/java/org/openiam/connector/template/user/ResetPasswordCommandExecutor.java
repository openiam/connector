package org.openiam.connector.template.user;

import org.openiam.api.connector.model.UserConnectorObject;
import org.openiam.api.connector.user.response.ResetPasswordUserProvisioningConnectorResponse;
import org.openiam.base.ws.ResponseStatus;
import org.openiam.connector.core.base.exception.ConnectorException;
import org.openiam.connector.template.base.AbstractTemplateConnectorCommand;
import org.springframework.stereotype.Component;

/**
 *
 * Change user password in target system (the possibility of this operation depends on the target system).
 * may be called during user change password (selfservice, forgot password feature or webconsole) in OpenIAM.
 *
 */
@Component("resetPasswordUserCommand")
public class ResetPasswordCommandExecutor extends AbstractTemplateConnectorCommand<UserConnectorObject, ResetPasswordUserProvisioningConnectorResponse> {

    @Override
    public ResetPasswordUserProvisioningConnectorResponse perform(UserConnectorObject connectorObject) throws ConnectorException {

        ResetPasswordUserProvisioningConnectorResponse response = new ResetPasswordUserProvisioningConnectorResponse();
        //TODO
        response.setStatus(ResponseStatus.SUCCESS);
        return response;
    }
}
