package org.openiam.connector.template.user;

import org.openiam.api.connector.model.UserConnectorObject;
import org.openiam.api.connector.user.response.ResumeUserProvisioningConnectorResponse;
import org.openiam.base.ws.ResponseStatus;
import org.openiam.connector.core.base.exception.ConnectorException;
import org.openiam.connector.template.base.AbstractTemplateConnectorCommand;
import org.springframework.stereotype.Component;

/**
 *
 * Change user status to enabled (the possibility of this operation depends on the target system).
 * may be called during change user status to enable  in OpenIAM.
 *
 */
@Component("resumeUserCommand")
public class ResumeUserCommandExecutor extends AbstractTemplateConnectorCommand<UserConnectorObject, ResumeUserProvisioningConnectorResponse> {

    @Override
    public ResumeUserProvisioningConnectorResponse perform(UserConnectorObject connectorObject) throws ConnectorException {
        ResumeUserProvisioningConnectorResponse response = new ResumeUserProvisioningConnectorResponse();
        response.setStatus(ResponseStatus.SUCCESS);
        //TODO
        return response;
    }
}
