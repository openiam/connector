package org.openiam.connector.template.user;

import org.openiam.api.connector.model.UserConnectorObject;
import org.openiam.api.connector.user.response.SearchUserProvisioningConnectorResponse;
import org.openiam.connector.core.base.exception.ConnectorException;
import org.openiam.connector.template.base.AbstractTemplateConnectorCommand;
import org.springframework.stereotype.Component;

/**
 *
 * Searching for users in the target system.
 * can be called during user synchronization with the target system.
 *
 */
@Component("searchUserCommand")
public class SearchUserCommandExecutor extends AbstractTemplateConnectorCommand<UserConnectorObject, SearchUserProvisioningConnectorResponse> {
    @Override
    public SearchUserProvisioningConnectorResponse perform(UserConnectorObject userConnectorObject) throws ConnectorException {
        SearchUserProvisioningConnectorResponse response = new SearchUserProvisioningConnectorResponse();
        //TODO
        return response;
    }
}
