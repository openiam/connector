package org.openiam.connector.template.user;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openiam.api.connector.model.ConnectorObject;
import org.openiam.api.connector.model.UserConnectorObject;
import org.openiam.api.connector.user.response.SaveUserProvisioningConnectorResponse;
import org.openiam.base.ws.ResponseStatus;
import org.openiam.connector.core.base.exception.ConnectorException;
import org.openiam.connector.template.base.AbstractTemplateConnectorCommand;
import org.springframework.stereotype.Component;


/**
 *
 * Create or update user in target system.
 * may be called during user creation or update user information (seflservice or webconsole) in OpenIAM.
 *
 */
@Component("saveUserCommand")
public class SaveUserCommandExecutor extends AbstractTemplateConnectorCommand<UserConnectorObject, SaveUserProvisioningConnectorResponse> {

    private static final Log log = LogFactory.getLog(SaveUserCommandExecutor.class);

    @Override
    public SaveUserProvisioningConnectorResponse perform(UserConnectorObject userConnectorObject) throws ConnectorException {
        SaveUserProvisioningConnectorResponse response = new SaveUserProvisioningConnectorResponse();
        ConnectorObject connectorObject = this.lookupUser(userConnectorObject);
        try {
            if (connectorObject == null) {
                doAdd(userConnectorObject);
                //add
            } else {
                //modify
                doUpdate(userConnectorObject);
            }
            response.setStatus(ResponseStatus.SUCCESS);
        } catch (Exception e) {
            log.error(e);
        }
        return response;
    }


    private void doAdd(UserConnectorObject userConnectorObject)
            throws ConnectorException {
       //TODO
    }


    private void doUpdate(UserConnectorObject userConnectorObject)
            throws ConnectorException {
        //TODO
    }
}
