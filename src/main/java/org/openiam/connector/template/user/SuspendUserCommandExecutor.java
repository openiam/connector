package org.openiam.connector.template.user;

import org.openiam.api.connector.model.UserConnectorObject;
import org.openiam.api.connector.user.response.SuspendUserProvisioningConnectorResponse;
import org.openiam.base.ws.ResponseStatus;
import org.openiam.connector.core.base.exception.ConnectorException;
import org.openiam.connector.template.base.AbstractTemplateConnectorCommand;
import org.springframework.stereotype.Component;

/**
 *
 * Change user status to suspended or disabled (the possibility of this operation depends on the target system).
 * may be called during change user status to disable in OpenIAM.
 *
 */
@Component("suspendUserCommand")
public class SuspendUserCommandExecutor extends AbstractTemplateConnectorCommand<UserConnectorObject, SuspendUserProvisioningConnectorResponse> {
    @Override
    public SuspendUserProvisioningConnectorResponse perform(UserConnectorObject connectorObject) throws ConnectorException {
        SuspendUserProvisioningConnectorResponse response = new SuspendUserProvisioningConnectorResponse();
        response.setStatus(ResponseStatus.SUCCESS);
        //TODO
        return response;
    }
}
