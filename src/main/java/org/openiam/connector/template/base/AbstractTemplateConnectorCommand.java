package org.openiam.connector.template.base;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openiam.api.connector.helper.ConnectorObjectUtils;
import org.openiam.api.connector.model.ConnectorObject;
import org.openiam.api.connector.model.StringConnectorAttribute;
import org.openiam.api.connector.model.StringOperationalConnectorValue;
import org.openiam.api.connector.user.response.AbstractConnectorResponse;
import org.openiam.base.AttributeOperationEnum;
import org.openiam.connector.core.base.commands.AbstractCommandExecutor;
import org.openiam.connector.core.base.exception.ConnectorException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Abstract class containing base methods for connectors.
 * All classes from user package extend this class.
 *
 */
public abstract class AbstractTemplateConnectorCommand<Request extends ConnectorObject, Response extends AbstractConnectorResponse> extends AbstractCommandExecutor<Request, Response> {

    private static final Log log = LogFactory.getLog(AbstractTemplateConnectorCommand.class);

    /**
     * Can be used to implement user(s) search in the target system.
     * @param request
     * @return
     * @throws ConnectorException
     */
    protected ConnectorObject lookupUser(Request request) throws ConnectorException {
        ConnectorObject connectorObject = null;
        //TODO
        return connectorObject;
    }

    /**
     * Get set of user attribute names to be returned to the OpenIAM (can be used for sync).
     * @param request
     * @return
     */
    protected Set<String> getReturnAttributeNames(ConnectorObject request) {
        if (CollectionUtils.isNotEmpty(request.getAttributes())) {
            return ConnectorObjectUtils.getAllAttributeNames(request.getAttributes());
        }
        return null;
    }

    /**
     * Get StringConnectorAttribute with a specific name and value(s).
     * @param name attribute name.
     * @param values - attribute values.
     * @return
     */
    private StringConnectorAttribute getOpenIAMAttribute(String name, Object values) {
        if (values instanceof Collection) {
            List<String> valueList = new ArrayList<>();
            for (Object value : (Collection) values) {
                if (value != null) {
                    valueList.add(String.valueOf(value));
                    valueList.toArray();
                }
            }
            if (CollectionUtils.isNotEmpty(valueList)) {
                return getOpenIAMAttribute(name, valueList.toArray(new String[valueList.size()]));
            }
        }
        return null;
    }

    /**
     * Create a new StringConnectorAttribute with a specific name and value(s).
     * @param name - attribute name.
     * @param values - attribute value(s).
     * @return
     */
    private StringConnectorAttribute getOpenIAMAttribute(String name, String... values) {
        StringConnectorAttribute stringConnectorAttribute = new StringConnectorAttribute(name);
        if (values != null) {
            stringConnectorAttribute.addValues(Arrays.stream(values).
                    map(it -> new StringOperationalConnectorValue(it, AttributeOperationEnum.NO_CHANGE))
                    .collect(Collectors.toList()));
        }
        return stringConnectorAttribute;
    }

    /**
     * Get the first attribute value.
     * @param a - custom field or attribute of managed system.
     * @return
     */
    private String getValue(final StringConnectorAttribute a) {
        if (a == null || CollectionUtils.isEmpty(a.getValues())) {
            return null;
        } else {
            return a.getValues().get(0).getValue();
        }
    }

    /**
     * Get a list of attribute values.
     * @param a - custom field or attribute of managed system.
     * @return
     */
    private List<String> getValues(final StringConnectorAttribute a) {
        if (a == null || CollectionUtils.isEmpty(a.getValues())) {
            return new ArrayList<>();
        } else {
            return a.getValues().stream().map(StringOperationalConnectorValue::getValue).collect(Collectors.toList());
        }
    }
}
