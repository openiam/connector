# Build connector project.

To build a connector project and get executable JAR file need to run the following command in the terminal:

`mvn clean install -s conf/settings`

there are 3 variables in settings.xml:

`${env.OPENIAM_ARTIFACTORY_USER} - username for OpenIAM artifactory`

 `${env.OPENIAM_ARTIFACTORY_PASSWORD} - user password for OpenIAM artifactory`
 
 `${env.ARTIFACTORY_CONTEXT_URL} - URL to OpenIAM artifactory
`

and can be in Environment variables (eg. in .bashrc file). Compiled JAR file is in _target/_ directory (eg. _freshservice-openiam-rabbitmq.jar_).

# Deploy connector to the server.

Copy compiled connector JAR file to the server. Create _run.sh_ file to start the connector which will contain:

`#/bin/bash
 java -Dlogging.level.root=INFO -Dconfpath=/data/openiam/ -Dorg.openiam.connector.queue=${Connector_Queue}_Request -Dorg.openiam.connector.queueResponseName=${Connector_Queue}_Response -jar JAR_FILE_NAME.jar>logs.out&`
 
 where _${Connector_Queue}_ can be find in _webconsole -> Provisioning->Connectors->_ edit the connector which is connected with managed system and copy value from Connector Queue field.
 
 make the run.sh script executable:
 
 `sudo chmod +x run.sh`

and run the connector.

`./run.sh`

# Stop connector.

To stop connector execute the following command:

`sudo ps -ef | grep connector-file-name`

and kill existed process:

`sudo kill -9 processID`

# OpenIAM connector operations.

OpenIAM supports several operations with users and groups (in version 4.1.x, OpenIAM does not support the group provisioning process. This option will be added in version 4.2):

##### SAVE (SaveUserCommandExecutor.java):
OpenIAM use this operation for CREATE and UPDATE users in target system. First step of save operation is check of user existence in target system. OpenIAM generates SEARCH request and based on response connector chooses CREATE or UPDATE operation will be performed.

##### DELETE (DeleteUserCommandExecutor.java):
Delete user from target system eg by user ID. Before performing of DELETE operation OpenIAM checks user's existence in target system. If target system doesn't support complete deletion of user then operation SUSPEND could be performed.

##### RESET_PASSWORD (ResetPasswordCommandExecutor.java):
Change user password in the target system (some systems do not support this operation therefore you can skip this if you do not want to allow this operation).

##### RESUME/SUSPEND(ResumeUserCommandExecutor.java and SuspendUserCommandExecutor.java):
Active/deactivate user in target system or change user status if target system supports several types of statuses (some systems do not support this operation therefore you can skip this if you do not want to allow this operation).

##### SEARCH (SearchUserCommandExecutor.java):
OpenIAM use this for search users/groups/roles etc in target system for synchronization. Would be nice if target system search API could support queries (get one user by some attribute, get all users, get users, get users with pagination (start index and offset) etc).

##### TEST (TestConnectionCommandExecutor.java):
Used for test connection to target system to check that all entered data are correct and the connection is established. Many systems do not have a separate API for this operation, so use the simplest.


# For developers.

OpenIAM Connector - Spring boot application using maven to build a Java project. To send requests and responses to the connector RabbitMQ message broker is used. For development can use IDE like Inlellij Idea or Eclipse.

All request information is contained in the **UserConnectorObject** and **ConnectorObjectMetaData** objects. **UserConnectorObject** includes (_fields that are necessary for development_):


_identityValue_ - user principal name to save.

_originalIdentityValue_ - original principal name if it was changed.

_attributes_ - list of **StringConnectorAttribute** objects which contain user information that is selected based on Policy Map for Managed System.

_operation_ - **AttributeOperationEnum** enum with the operation that is performed.

_type_ - **ProvisioningObjectType** enum with object type.

_metaData_ - **ConnectorObjectMetaData** object with information from Managed System page.

**ConnectorObjectMetaData** includes fields filled on the Managed System page (_fields that are necessary for development_):

_url_ - base target system URL.

_login_ - user login to access the target system.

_password_ - user password.

_attributes_ - list of **StringConnectorAttribute** objects which contain custom Managed System fields and attributes.

For all operations performed there is a response class that extends **AbstractConnectorResponse** class. The main indicator of the fulfillment or non-fulfillment of a request is the **ResponseStatus**. 

For synchronization **SearchUserProvisioningConnectorResponse** contains userList field - list of **UserConnectorObject** objects which will be returned to OpenIAM system.


