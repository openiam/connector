
package org.openiam.connector.freshservice.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AgentUpdate implements Serializable {

    private static final long serialVersionUID = 53031391787932379L;

    @JsonProperty("email")
    private String email;
    @JsonProperty("reporting_manager_id")
    private String reportingManagerId;
    @JsonProperty("time_zone")
    private String timeZone;
    @JsonProperty("time_format")
    private String timeFormat;
    @JsonProperty("language")
    private String language;
    @JsonProperty("location_id")
    private Object locationId;
    @JsonProperty("scoreboard_level_id")
    private Object scoreboardLevelId;
    @JsonProperty("group_ids")
    private List<String> groupIds = null;
    @JsonProperty("role_ids")
    private List<String> roleIds = null;
    @JsonProperty("custom_fields")
    private CustomFields customFields;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getReportingManagerId() {
        return reportingManagerId;
    }

    public void setReportingManagerId(String reportingManagerId) {
        this.reportingManagerId = reportingManagerId;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getTimeFormat() {
        return timeFormat;
    }

    public void setTimeFormat(String timeFormat) {
        this.timeFormat = timeFormat;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Object getLocationId() {
        return locationId;
    }

    public void setLocationId(Object locationId) {
        this.locationId = locationId;
    }

    public Object getScoreboardLevelId() {
        return scoreboardLevelId;
    }

    public void setScoreboardLevelId(Object scoreboardLevelId) {
        this.scoreboardLevelId = scoreboardLevelId;
    }

    public List<String> getGroupIds() {
        return groupIds;
    }

    public void setGroupIds(List<String> groupIds) {
        this.groupIds = groupIds;
    }

    public List<String> getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(List<String> roleIds) {
        this.roleIds = roleIds;
    }

    public CustomFields getCustomFields() {
        return customFields;
    }

    public void setCustomFields(CustomFields customFields) {
        this.customFields = customFields;
    }
}
