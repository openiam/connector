
package org.openiam.connector.freshservice.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ticket",
    "problem",
    "change",
    "release"
})
public class Scopes implements Serializable {

    private static final long serialVersionUID = -1748707873457485976L;
    @JsonProperty("ticket")
    private String ticket;
    @JsonProperty("problem")
    private String problem;
    @JsonProperty("change")
    private String change;
    @JsonProperty("release")
    private String release;

    @JsonProperty("ticket")
    public String getTicket() {
        return ticket;
    }

    @JsonProperty("ticket")
    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    @JsonProperty("problem")
    public String getProblem() {
        return problem;
    }

    @JsonProperty("problem")
    public void setProblem(String problem) {
        this.problem = problem;
    }

    @JsonProperty("change")
    public String getChange() {
        return change;
    }

    @JsonProperty("change")
    public void setChange(String change) {
        this.change = change;
    }

    @JsonProperty("release")
    public String getRelease() {
        return release;
    }

    @JsonProperty("release")
    public void setRelease(String release) {
        this.release = release;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(ticket).append(change).append(problem).append(release).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Scopes) == false) {
            return false;
        }
        Scopes rhs = ((Scopes) other);
        return new EqualsBuilder().append(ticket, rhs.ticket).append(change, rhs.change).append(problem, rhs.problem).append(release, rhs.release).isEquals();
    }

}
