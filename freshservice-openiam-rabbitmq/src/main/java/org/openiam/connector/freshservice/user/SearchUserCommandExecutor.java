package org.openiam.connector.freshservice.user;

import org.openiam.api.connector.model.UserConnectorObject;
import org.openiam.api.connector.user.response.SearchUserProvisioningConnectorResponse;
import org.openiam.connector.core.base.exception.ConnectorException;
import org.openiam.connector.freshservice.base.AbstractFreshserviceConnectorCommand;
import org.springframework.stereotype.Component;

@Component("searchUserCommand")
public class SearchUserCommandExecutor extends AbstractFreshserviceConnectorCommand<UserConnectorObject, SearchUserProvisioningConnectorResponse> {
    @Override
    public SearchUserProvisioningConnectorResponse perform(UserConnectorObject userConnectorObject) throws ConnectorException {
        SearchUserProvisioningConnectorResponse response = new SearchUserProvisioningConnectorResponse();
        //TODO
        return response;
    }
}
