package org.openiam.connector.freshservice.user;

import org.openiam.api.connector.model.UserConnectorObject;
import org.openiam.api.connector.user.response.ResetPasswordUserProvisioningConnectorResponse;
import org.openiam.connector.core.base.exception.ConnectorErrorCode;
import org.openiam.connector.core.base.exception.ConnectorException;
import org.openiam.connector.freshservice.base.AbstractFreshserviceConnectorCommand;
import org.springframework.stereotype.Component;

import javax.naming.OperationNotSupportedException;

@Component("resetPasswordUserCommand")
public class ResetPasswordCommandExecutor extends AbstractFreshserviceConnectorCommand<UserConnectorObject, ResetPasswordUserProvisioningConnectorResponse> {

    @Override
    public ResetPasswordUserProvisioningConnectorResponse perform(UserConnectorObject connectorObject) throws ConnectorException {
        throw new ConnectorException(ConnectorErrorCode.OPERATION_NOT_SUPPORTED, new OperationNotSupportedException("Reset password is not applied for freshservice"));
    }
}
