package org.openiam.connector.freshservice.user;

import org.openiam.api.connector.model.UserConnectorObject;
import org.openiam.api.connector.user.response.SuspendUserProvisioningConnectorResponse;
import org.openiam.connector.core.base.exception.ConnectorErrorCode;
import org.openiam.connector.core.base.exception.ConnectorException;
import org.openiam.connector.freshservice.base.AbstractFreshserviceConnectorCommand;
import org.springframework.stereotype.Component;

import javax.naming.OperationNotSupportedException;

@Component("suspendUserCommand")
public class SuspendUserCommandExecutor extends AbstractFreshserviceConnectorCommand<UserConnectorObject, SuspendUserProvisioningConnectorResponse> {
    @Override
    public SuspendUserProvisioningConnectorResponse perform(UserConnectorObject connectorObject) throws ConnectorException {
        throw new ConnectorException(ConnectorErrorCode.OPERATION_NOT_SUPPORTED, new OperationNotSupportedException("Suspend is not applied for freshservice"));
    }
}
