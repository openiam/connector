
package org.openiam.connector.freshservice.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "agent"
})
public class AgentFreshserviceResponse implements Serializable {

    private static final long serialVersionUID = 7892883320845905922L;
    @JsonProperty("agent")
    private Agent agent;

    @JsonProperty("agent")
    public Agent getAgent() {
        return agent;
    }

    @JsonProperty("agent")
    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(agent).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof AgentFreshserviceResponse) == false) {
            return false;
        }
        AgentFreshserviceResponse rhs = ((AgentFreshserviceResponse) other);
        return new EqualsBuilder().append(agent, rhs.agent).isEquals();
    }

}
