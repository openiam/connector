package org.openiam.connector.freshservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class FreshserviceUser implements Serializable {
    private static final long serialVersionUID = 8202554293987466544L;

    private String id;
    @JsonProperty(required = true)
    private String name;
    @JsonProperty(required = true)
    private String email;
    private String address;
    private String description;
    @JsonProperty("job_title")
    private String jobTitle;
    private String phone;
    private String mobile;
    private String language;
    @JsonProperty("time_zone")
    private String timeZone;

    private boolean deleted;
    @JsonProperty("helpdesk_agent")
    private boolean helpdeskAgent;

    private boolean active;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean getHelpdeskAgent() {
        return helpdeskAgent;
    }

    public void setHelpdeskAgent(boolean helpdeskAgent) {
        this.helpdeskAgent = helpdeskAgent;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
