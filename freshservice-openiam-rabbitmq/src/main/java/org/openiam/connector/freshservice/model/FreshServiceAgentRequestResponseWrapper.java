package org.openiam.connector.freshservice.model;

import java.io.Serializable;

public class FreshServiceAgentRequestResponseWrapper implements Serializable {
    private static final long serialVersionUID = 2375736686568188472L;
    private FreshserviceAgent agent;

    public FreshserviceAgent getAgent() {
        return agent;
    }

    public void setAgent(FreshserviceAgent agent) {
        this.agent = agent;
    }
}
