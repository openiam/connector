package org.openiam.connector.freshservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FreshserviceAgent implements Serializable {
    private static final long serialVersionUID = 8000054293987466544L;
//
//      "active_since":null,
//              "available":true,
//              "created_at":"2015-01-02T22:56:39-10:00",
//              "id":19,
//              "occasional":false,
//              "points":2500,
//              "scoreboard_level_id":5,
//              "signature":null,
//              "signature_html":"\u003Cp\u003E\u003Cbr\u003E\u003C/p\u003E\r\n",
//              "ticket_permission":1,
//              "updated_at":"2015-01-04T23:09:52-10:00",

    @JsonProperty(required = true)
    private String id;
    @JsonProperty(required = true)
    private FreshserviceUser user;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public FreshserviceUser getUser() {
        return user;
    }

    public void setUser(FreshserviceUser user) {
        this.user = user;
    }
}
