package org.openiam.connector.freshservice.base;

import org.springframework.http.HttpMethod;

import javax.validation.constraints.NotNull;

public enum FreshserviceAPI {

    CREATE_USER(HttpMethod.POST, "/itil/requesters.json"),
    GET_USER_BY_ID(HttpMethod.GET, "/itil/requesters/%s.json"),
    GET_AGENT_BY_ID(HttpMethod.GET, "/api/v2/agents/%s"),
    LIST_REQUESTER_USERS(HttpMethod.GET, "/itil/requesters.json"),
    LIST_AGENT_USERS(HttpMethod.GET, "/agents.json"),
    UPDATE_USER(HttpMethod.PUT, "/itil/requesters/%s.json"),
    UPDATE_AGENT(HttpMethod.PUT, "/api/v2/agents/%s"),
    DELETE_USER(HttpMethod.DELETE, "/itil/requesters/%s.json"),
    DELETE_AGENT(HttpMethod.DELETE, "/api/v2/agents/%s");

    private final HttpMethod httpMethod;
    private final String endpoint;

    FreshserviceAPI(HttpMethod httpMethod, String endpoint) {
        this.endpoint = endpoint;
        this.httpMethod = httpMethod;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public String getEndpoint(@NotNull String... objects) {
        return String.format(endpoint, objects);
    }

    public HttpMethod getHttpMethod() {
        return httpMethod;
    }

}
