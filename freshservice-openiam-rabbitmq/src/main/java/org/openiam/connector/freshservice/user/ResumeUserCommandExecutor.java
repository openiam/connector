package org.openiam.connector.freshservice.user;

import org.openiam.api.connector.model.UserConnectorObject;
import org.openiam.api.connector.user.response.ResumeUserProvisioningConnectorResponse;
import org.openiam.connector.core.base.exception.ConnectorErrorCode;
import org.openiam.connector.core.base.exception.ConnectorException;
import org.openiam.connector.freshservice.base.AbstractFreshserviceConnectorCommand;
import org.springframework.stereotype.Component;

import javax.naming.OperationNotSupportedException;

@Component("resumeUserCommand")
public class ResumeUserCommandExecutor extends AbstractFreshserviceConnectorCommand<UserConnectorObject, ResumeUserProvisioningConnectorResponse> {

    @Override
    public ResumeUserProvisioningConnectorResponse perform(UserConnectorObject connectorObject) throws ConnectorException {
        throw new ConnectorException(ConnectorErrorCode.OPERATION_NOT_SUPPORTED, new OperationNotSupportedException("Resume is not applied for freshservice"));
    }
}
