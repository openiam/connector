package org.openiam.connector.freshservice.model;

import java.io.Serializable;

public class FreshServiceRequestResponseWrapper implements Serializable {
    private static final long serialVersionUID = 2675736686568188472L;
    private FreshserviceUser user;

    public FreshserviceUser getUser() {
        return user;
    }

    public void setUser(FreshserviceUser user) {
        this.user = user;
    }
}
