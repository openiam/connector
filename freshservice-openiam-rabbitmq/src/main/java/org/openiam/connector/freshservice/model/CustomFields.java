
package org.openiam.connector.freshservice.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "untitled",
    "user_type",
    "u_start_date",
    "u_end_date",
    "world_reg",
    "u_loc",
    "u_job_change"
})
public class CustomFields implements Serializable {

    private static final long serialVersionUID = -536886980602757758L;
    @JsonProperty("untitled")
    private Object untitled;
    @JsonProperty("user_type")
    private Object userType;
    @JsonProperty("u_start_date")
    private Object uStartDate;
    @JsonProperty("u_end_date")
    private Object uEndDate;
    @JsonProperty("world_reg")
    private Object worldReg;
    @JsonProperty("u_loc")
    private String uLoc;
    @JsonProperty("u_job_change")
    private Object uJobChange;

    @JsonProperty("untitled")
    public Object getUntitled() {
        return untitled;
    }

    @JsonProperty("untitled")
    public void setUntitled(Object untitled) {
        this.untitled = untitled;
    }

    @JsonProperty("user_type")
    public Object getUserType() {
        return userType;
    }

    @JsonProperty("user_type")
    public void setUserType(Object userType) {
        this.userType = userType;
    }

    @JsonProperty("u_start_date")
    public Object getUStartDate() {
        return uStartDate;
    }

    @JsonProperty("u_start_date")
    public void setUStartDate(Object uStartDate) {
        this.uStartDate = uStartDate;
    }

    @JsonProperty("u_end_date")
    public Object getUEndDate() {
        return uEndDate;
    }

    @JsonProperty("u_end_date")
    public void setUEndDate(Object uEndDate) {
        this.uEndDate = uEndDate;
    }

    @JsonProperty("world_reg")
    public Object getWorldReg() {
        return worldReg;
    }

    @JsonProperty("world_reg")
    public void setWorldReg(Object worldReg) {
        this.worldReg = worldReg;
    }

    @JsonProperty("u_loc")
    public String getULoc() {
        return uLoc;
    }

    @JsonProperty("u_loc")
    public void setULoc(String uLoc) {
        this.uLoc = uLoc;
    }

    @JsonProperty("u_job_change")
    public Object getUJobChange() {
        return uJobChange;
    }

    @JsonProperty("u_job_change")
    public void setUJobChange(Object uJobChange) {
        this.uJobChange = uJobChange;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(uLoc).append(worldReg).append(uJobChange).append(uEndDate).append(untitled).append(uStartDate).append(userType).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof CustomFields) == false) {
            return false;
        }
        CustomFields rhs = ((CustomFields) other);
        return new EqualsBuilder().append(uLoc, rhs.uLoc).append(worldReg, rhs.worldReg).append(uJobChange, rhs.uJobChange).append(uEndDate, rhs.uEndDate).append(untitled, rhs.untitled).append(uStartDate, rhs.uStartDate).append(userType, rhs.userType).isEquals();
    }

}
