package org.openiam.connector.freshservice;

import com.github.vanroy.springboot.autoconfigure.data.jest.ElasticsearchJestAutoConfiguration;
import com.github.vanroy.springboot.autoconfigure.data.jest.ElasticsearchJestDataAutoConfiguration;
import org.openiam.connector.core.config.RabbitConfig;
import org.openiam.spring.property.OpeniamPropertySourceFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchAutoConfiguration;
import org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchDataAutoConfiguration;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.elasticsearch.jest.JestAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.web.EmbeddedServletContainerAutoConfiguration;
import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.web.client.RestTemplate;

@Configuration
@SpringBootApplication(exclude = {
        EmbeddedServletContainerAutoConfiguration.class,
        WebMvcAutoConfiguration.class,
        DataSourceAutoConfiguration.class,
        DataSourceTransactionManagerAutoConfiguration.class,
        ElasticsearchDataAutoConfiguration.class,
        ElasticsearchAutoConfiguration.class,
        ElasticsearchJestAutoConfiguration.class,
        ElasticsearchJestDataAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class,
        JpaRepositoriesAutoConfiguration.class,
        RedisAutoConfiguration.class,
        RedisRepositoriesAutoConfiguration.class,
        JestAutoConfiguration.class
})
@Import(RabbitConfig.class)
@ComponentScan(basePackages = {
        "org.openiam.connector.freshservice",
        "org.openiam.common.beans.health"
}, excludeFilters = {})
@PropertySources({
        @PropertySource(ignoreResourceNotFound = true, value = {
                "file:${confpath}/conf/freshservice-connector-rabbitmq.properties"
        }, factory = OpeniamPropertySourceFactory.class)
})
public class Application {
    public static void main(final String[] args) {
        SpringApplication springApplication =
                new SpringApplicationBuilder()
                        .sources(Application.class)
                        .web(false)
                        .build();
        springApplication.run(args);
    }

    @Bean
    RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
