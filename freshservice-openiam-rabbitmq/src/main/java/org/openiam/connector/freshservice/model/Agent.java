
package org.openiam.connector.freshservice.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "first_name",
    "last_name",
    "occasional",
    "job_title",
    "email",
    "work_phone_number",
    "mobile_phone_number",
    "reporting_manager_id",
    "signature",
    "time_zone",
    "time_format",
    "language",
    "location_id",
    "scoreboard_level_id",
    "scopes",
    "group_ids",
    "role_ids",
    "last_login_at",
    "custom_fields"
})
public class Agent implements Serializable {

    private static final long serialVersionUID = -1053031391787932379L;
    @JsonProperty("id")
    private String id;
    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("last_name")
    private String lastName;
    @JsonProperty("occasional")
    private Boolean occasional;
    @JsonProperty("job_title")
    private String jobTitle;
    @JsonProperty("email")
    private String email;
    @JsonProperty("work_phone_number")
    private String workPhoneNumber;
    @JsonProperty("mobile_phone_number")
    private String mobilePhoneNumber;
    @JsonProperty("reporting_manager_id")
    private String reportingManagerId;
    @JsonProperty("signature")
    private String signature;
    @JsonProperty("time_zone")
    private String timeZone;
    @JsonProperty("time_format")
    private String timeFormat;
    @JsonProperty("language")
    private String language;
    @JsonProperty("location_id")
    private Object locationId;
    @JsonProperty("scoreboard_level_id")
    private Object scoreboardLevelId;
    @JsonProperty("scopes")
    private Scopes scopes;
    @JsonProperty("group_ids")
    private List<String> groupIds = null;
    @JsonProperty("role_ids")
    private List<String> roleIds = null;
    @JsonProperty("last_login_at")
    private String lastLoginAt;
    @JsonProperty("custom_fields")
    private CustomFields customFields;

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("first_name")
    public String getFirstName() {
        return firstName;
    }

    @JsonProperty("first_name")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @JsonProperty("last_name")
    public String getLastName() {
        return lastName;
    }

    @JsonProperty("last_name")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @JsonProperty("occasional")
    public Boolean getOccasional() {
        return occasional;
    }

    @JsonProperty("occasional")
    public void setOccasional(Boolean occasional) {
        this.occasional = occasional;
    }

    @JsonProperty("job_title")
    public String getJobTitle() {
        return jobTitle;
    }

    @JsonProperty("job_title")
    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("work_phone_number")
    public String getWorkPhoneNumber() {
        return workPhoneNumber;
    }

    @JsonProperty("work_phone_number")
    public void setWorkPhoneNumber(String workPhoneNumber) {
        this.workPhoneNumber = workPhoneNumber;
    }

    @JsonProperty("mobile_phone_number")
    public String getMobilePhoneNumber() {
        return mobilePhoneNumber;
    }

    @JsonProperty("mobile_phone_number")
    public void setMobilePhoneNumber(String mobilePhoneNumber) {
        this.mobilePhoneNumber = mobilePhoneNumber;
    }

    @JsonProperty("reporting_manager_id")
    public String getReportingManagerId() {
        return reportingManagerId;
    }

    @JsonProperty("reporting_manager_id")
    public void setReportingManagerId(String reportingManagerId) {
        this.reportingManagerId = reportingManagerId;
    }

    @JsonProperty("signature")
    public String getSignature() {
        return signature;
    }

    @JsonProperty("signature")
    public void setSignature(String signature) {
        this.signature = signature;
    }

    @JsonProperty("time_zone")
    public String getTimeZone() {
        return timeZone;
    }

    @JsonProperty("time_zone")
    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    @JsonProperty("time_format")
    public String getTimeFormat() {
        return timeFormat;
    }

    @JsonProperty("time_format")
    public void setTimeFormat(String timeFormat) {
        this.timeFormat = timeFormat;
    }

    @JsonProperty("language")
    public String getLanguage() {
        return language;
    }

    @JsonProperty("language")
    public void setLanguage(String language) {
        this.language = language;
    }

    @JsonProperty("location_id")
    public Object getLocationId() {
        return locationId;
    }

    @JsonProperty("location_id")
    public void setLocationId(Object locationId) {
        this.locationId = locationId;
    }

    @JsonProperty("scoreboard_level_id")
    public Object getScoreboardLevelId() {
        return scoreboardLevelId;
    }

    @JsonProperty("scoreboard_level_id")
    public void setScoreboardLevelId(Object scoreboardLevelId) {
        this.scoreboardLevelId = scoreboardLevelId;
    }

    @JsonProperty("scopes")
    public Scopes getScopes() {
        return scopes;
    }

    @JsonProperty("scopes")
    public void setScopes(Scopes scopes) {
        this.scopes = scopes;
    }

    @JsonProperty("group_ids")
    public List<String> getGroupIds() {
        return groupIds;
    }

    @JsonProperty("group_ids")
    public void setGroupIds(List<String> groupIds) {
        this.groupIds = groupIds;
    }

    @JsonProperty("role_ids")
    public List<String> getRoleIds() {
        return roleIds;
    }

    @JsonProperty("role_ids")
    public void setRoleIds(List<String> roleIds) {
        this.roleIds = roleIds;
    }

    @JsonProperty("last_login_at")
    public String getLastLoginAt() {
        return lastLoginAt;
    }

    @JsonProperty("last_login_at")
    public void setLastLoginAt(String lastLoginAt) {
        this.lastLoginAt = lastLoginAt;
    }

    @JsonProperty("custom_fields")
    public CustomFields getCustomFields() {
        return customFields;
    }

    @JsonProperty("custom_fields")
    public void setCustomFields(CustomFields customFields) {
        this.customFields = customFields;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(lastName).append(lastLoginAt).append(customFields).append(scoreboardLevelId).append(locationId).append(reportingManagerId).append(timeFormat).append(timeZone).append(workPhoneNumber).append(mobilePhoneNumber).append(occasional).append(id).append(scopes).append(groupIds).append(email).append(language).append(firstName).append(roleIds).append(signature).append(jobTitle).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Agent) == false) {
            return false;
        }
        Agent rhs = ((Agent) other);
        return new EqualsBuilder().append(lastName, rhs.lastName).append(lastLoginAt, rhs.lastLoginAt).append(customFields, rhs.customFields).append(scoreboardLevelId, rhs.scoreboardLevelId).append(locationId, rhs.locationId).append(reportingManagerId, rhs.reportingManagerId).append(timeFormat, rhs.timeFormat).append(timeZone, rhs.timeZone).append(workPhoneNumber, rhs.workPhoneNumber).append(mobilePhoneNumber, rhs.mobilePhoneNumber).append(occasional, rhs.occasional).append(id, rhs.id).append(scopes, rhs.scopes).append(groupIds, rhs.groupIds).append(email, rhs.email).append(language, rhs.language).append(firstName, rhs.firstName).append(roleIds, rhs.roleIds).append(signature, rhs.signature).append(jobTitle, rhs.jobTitle).isEquals();
    }

}
