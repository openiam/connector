package org.openiam.connector.freshservice.base;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.client.utils.URIBuilder;
import org.openiam.api.connector.helper.ConnectorObjectUtils;
import org.openiam.api.connector.model.ConnectorObject;
import org.openiam.api.connector.model.ConnectorObjectMetaData;
import org.openiam.api.connector.model.StringConnectorAttribute;
import org.openiam.api.connector.model.StringOperationalConnectorValue;
import org.openiam.api.connector.model.UserConnectorObject;
import org.openiam.api.connector.user.response.AbstractConnectorResponse;
import org.openiam.base.AttributeOperationEnum;
import org.openiam.connector.core.base.commands.AbstractCommandExecutor;
import org.openiam.connector.core.base.exception.ConnectorException;
import org.openiam.connector.freshservice.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class AbstractFreshserviceConnectorCommand<Request extends ConnectorObject, Response extends AbstractConnectorResponse> extends AbstractCommandExecutor<Request, Response> {

    @Autowired
    protected RestTemplate restTemplate;

    private static final Log log = LogFactory.getLog(AbstractFreshserviceConnectorCommand.class);
    /*    private static final String endpointURL = "https://openiam.freshdesk.com";
        private static final String token = "GHABpJEMKp9XfxImgB";*/


    protected String existInFreshservice(UserConnectorObject connectorObject, FreshserviceAPI api) throws Exception {
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("query"
                , String.format("%s is %s", connectorObject.getIdentityName(), connectorObject.getOriginalIdentityValue() == null
                        ? connectorObject.getIdentityValue() :
                        connectorObject.getOriginalIdentityValue()));

        String retVal = null;
        switch (api) {
            case LIST_AGENT_USERS:
                List<FreshServiceAgentRequestResponseWrapper> agents = exchangeList(api, connectorObject, queryParams, null, null,
                        new ParameterizedTypeReference<List<FreshServiceAgentRequestResponseWrapper>>() {
                        });
                FreshServiceAgentRequestResponseWrapper response = agents.stream().findFirst().orElse(null);
                if (response != null && response.getAgent() != null) {
                    retVal = response.getAgent().getUser().getId();
                }
                break;
            case LIST_REQUESTER_USERS:
                List<FreshServiceRequestResponseWrapper> data = exchangeList(api, connectorObject, queryParams, null, null);
                FreshServiceRequestResponseWrapper responseWrapper = data.stream().findFirst().orElse(null);
                retVal = responseWrapper == null ? null : responseWrapper.getUser().getId();
                break;
            default:
                break;
        }
        return retVal;
    }

    private HttpHeaders createHeaders(String username, String password) {
        return new FreshserviceHttpHeaders(username, password);
    }

    protected List<FreshServiceRequestResponseWrapper> exchangeList(FreshserviceAPI api,
                                                                    UserConnectorObject connectorObject,
                                                                    Map<String, String> queryParams,
                                                                    String[] pathParams,
                                                                    FreshServiceRequestResponseWrapper payload) throws Exception {
        return exchangeList(api, connectorObject, queryParams, pathParams, payload, new ParameterizedTypeReference<List<FreshServiceRequestResponseWrapper>>() {
        });
    }

    protected <T extends Serializable> List<T> exchangeList(FreshserviceAPI api,
                                                            UserConnectorObject connectorObject,
                                                            Map<String, String> queryParams,
                                                            String[] pathParams,
                                                            FreshServiceRequestResponseWrapper payload,
                                                            ParameterizedTypeReference<List<T>> responseClass) throws Exception {
        final ConnectorObjectMetaData metaData = connectorObject.getMetaData();
        return exchangeList(api,
                metaData.getUrl(),
                metaData.getPassword(),
                "X",
                queryParams,
                pathParams,
                payload, responseClass
        );
    }

    protected List<FreshServiceRequestResponseWrapper> exchangeList(FreshserviceAPI api,
                                                                    String host,
                                                                    String userName,
                                                                    String password,
                                                                    Map<String, String> queryParams,
                                                                    String[] pathParams,
                                                                    FreshServiceRequestResponseWrapper payload
    ) throws Exception {
        return exchangeList(api, host, userName, password, queryParams, pathParams, payload, new ParameterizedTypeReference<List<FreshServiceRequestResponseWrapper>>() {
        });
    }

    protected <T extends Serializable> List<T> exchangeList(FreshserviceAPI api,
                                                            String host,
                                                            String userName,
                                                            String password,
                                                            Map<String, String> queryParams,
                                                            String[] pathParams,
                                                            FreshServiceRequestResponseWrapper payload,
                                                            ParameterizedTypeReference<List<T>> responseClass
    ) throws Exception {
        URIBuilder builder = new URIBuilder(host)
                .setPath(pathParams == null ? api.getEndpoint() : api.getEndpoint(pathParams));
        if (queryParams != null) {
            queryParams.forEach(builder::addParameter);
        }

        final ResponseEntity<List<T>> responseEntity = restTemplate
                .exchange(builder.build(), api.getHttpMethod(),
                        new HttpEntity<>(payload, createHeaders(userName, password)),
                        responseClass);

        if (responseEntity.getStatusCodeValue() == 200) {
            return responseEntity.getBody();
        } else {
            throw new Exception(responseEntity.getStatusCode().toString());
        }
    }


    protected FreshServiceRequestResponseWrapper exchange(FreshserviceAPI api,
                                                          UserConnectorObject connectorObject,
                                                          Map<String, String> queryParams,
                                                          String[] pathParams,
                                                          FreshServiceRequestResponseWrapper payload) throws Exception {
        final ConnectorObjectMetaData metaData = connectorObject.getMetaData();
        return exchange(api,
                metaData.getUrl(),
                metaData.getPassword(),
                "X",
                queryParams,
                pathParams,
                payload, FreshServiceRequestResponseWrapper.class
        );
    }

    protected <T extends Serializable> T exchange(FreshserviceAPI api,
                                                  UserConnectorObject connectorObject,
                                                  Map<String, String> queryParams,
                                                  String[] pathParams,
                                                  T payload,
                                                  Class<T> responseClass) throws Exception {
        final ConnectorObjectMetaData metaData = connectorObject.getMetaData();
        return exchange(api,
                metaData.getUrl(),
                metaData.getPassword(),
                "X",
                queryParams,
                pathParams,
                payload,
                responseClass
        );
    }

    protected <T extends Serializable> T exchange(FreshserviceAPI api,
                                                  String host,
                                                  String userName,
                                                  String password,
                                                  Map<String, String> queryParams,
                                                  String[] pathParams,
                                                  T payload,
                                                  Class<T> responseClass
    ) throws Exception {
        URIBuilder builder = new URIBuilder(host)
                .setPath(pathParams == null ? api.getEndpoint() : api.getEndpoint(pathParams));
        if (queryParams != null) {
            queryParams.forEach(builder::addParameter);
        }
        if (HttpMethod.DELETE.equals(api.getHttpMethod())) {
            //for delete this is fine.
            final ResponseEntity<String> responseEntity = restTemplate
                    .exchange(builder.build(), api.getHttpMethod(),
                            new HttpEntity<>(payload, createHeaders(userName, password)),
                            String.class);
            if (responseEntity.getStatusCodeValue() == 200) {
                return null;
            } else {
                throw new Exception(responseEntity.getStatusCode().toString());
            }
        } else {
            final ResponseEntity<T> responseEntity = restTemplate
                    .exchange(builder.build(), api.getHttpMethod(),
                            new HttpEntity<>(payload, createHeaders(userName, password)),
                            responseClass);

            if (responseEntity.getStatusCodeValue() == 200) {
                return responseEntity.getBody();
            } else {
                throw new Exception(responseEntity.getStatusCode().toString());
            }
        }
    }

    protected FreshserviceUser extensibleUserToFreshservice(UserConnectorObject user) {
        FreshserviceUser obj = new FreshserviceUser();
        for (final StringConnectorAttribute a : user.getAttributes()) {
            if (StringUtils.isNotBlank(a.getName()))
                switch (a.getName().trim()) {
                    case "name":
                        obj.setName(getValue(a));
                        break;
                    case "email":
                        obj.setEmail(getValue(a));
                        break;
                    case "phone":
                        obj.setPhone(getValue(a));
                        break;
                    case "mobile":
                        obj.setMobile(getValue(a));
                        break;
                    case "address":
                        obj.setAddress(getValue(a));
                        break;
                    default:
                        break;
                }
        }
        return obj;
    }

    protected void merge(UserConnectorObject user, AgentUpdate obj) {
        for (final StringConnectorAttribute a : user.getAttributes()) {
            if (StringUtils.isNotBlank(a.getName()))
                switch (a.getName().trim()) {
                    case "email":
                        obj.setEmail(getValueOrDelete(a));
                        break;
                    default:
                        break;
                }
        }
    }

    protected void merge(UserConnectorObject user, FreshserviceUser obj) {
        for (final StringConnectorAttribute a : user.getAttributes()) {
            if (StringUtils.isNotBlank(a.getName()))
                switch (a.getName().trim()) {
                    case "name":
                        obj.setName(getValueOrDelete(a));
                        break;
                    case "email":
                        obj.setEmail(getValueOrDelete(a));
                        break;
                    case "phone":
                        obj.setPhone(getValueOrDelete(a));
                        break;
                    case "mobile":
                        obj.setMobile(getValueOrDelete(a));
                        break;
                    case "address":
                        obj.setAddress(getValueOrDelete(a));
                        break;
                    default:
                        break;
                }
        }
    }

    protected ConnectorObject lookupUser(Request request) throws ConnectorException {
        ConnectorObject connectorObject = null;
        //TODO

        return connectorObject;
    }

    protected Set<String> getReturnAttributeNames(ConnectorObject request) {
        if (CollectionUtils.isNotEmpty(request.getAttributes())) {
            return ConnectorObjectUtils.getAllAttributeNames(request.getAttributes());
        }
        return null;
    }

    private StringConnectorAttribute getOpenIAMAttribute(String name, Object values) {
        if (values instanceof Collection) {
            List<String> valueList = new ArrayList<>();
            for (Object value : (Collection) values) {
                if (value != null) {
                    valueList.add(String.valueOf(value));
                    valueList.toArray();
                }
            }
            if (CollectionUtils.isNotEmpty(valueList)) {
                return getOpenIAMAttribute(name, valueList.toArray(new String[valueList.size()]));
            }
        }
        return null;
    }

    private StringConnectorAttribute getOpenIAMAttribute(String name, String... values) {
        StringConnectorAttribute stringConnectorAttribute = new StringConnectorAttribute(name);
        if (values != null) {
            stringConnectorAttribute.addValues(Arrays.stream(values).
                    map(it -> new StringOperationalConnectorValue(it, AttributeOperationEnum.NO_CHANGE))
                    .collect(Collectors.toList()));
        }
        return stringConnectorAttribute;
    }

    private String getValue(final StringConnectorAttribute a) {
        if (a == null || CollectionUtils.isEmpty(a.getValues())) {
            return null;
        } else {
            return a.getValues().get(0).getValue();
        }
    }

    private String getValueOrDelete(final StringConnectorAttribute a) {
        if (a == null || CollectionUtils.isEmpty(a.getValues())) {
            return null;
        } else {
            return AttributeOperationEnum.DELETE.equals(a.getValues().get(0)) ? null : a.getValues().get(0).
                    getValue();
        }
    }

    private List<String> getValues(final StringConnectorAttribute a) {
        if (a == null || CollectionUtils.isEmpty(a.getValues())) {
            return new ArrayList<>();
        } else {
            return a.getValues().stream().map(StringOperationalConnectorValue::getValue).collect(Collectors.toList());
        }
    }
}
