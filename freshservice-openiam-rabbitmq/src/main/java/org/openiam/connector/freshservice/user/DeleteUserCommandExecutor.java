package org.openiam.connector.freshservice.user;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openiam.api.connector.model.UserConnectorObject;
import org.openiam.api.connector.user.response.DeleteUserProvisioningConnectorResponse;
import org.openiam.base.ws.ResponseCode;
import org.openiam.base.ws.ResponseStatus;
import org.openiam.connector.core.base.exception.ConnectorException;
import org.openiam.connector.freshservice.base.AbstractFreshserviceConnectorCommand;
import org.openiam.connector.freshservice.base.FreshserviceAPI;
import org.springframework.stereotype.Component;

@Component("deleteUserCommand")
public class DeleteUserCommandExecutor extends AbstractFreshserviceConnectorCommand<UserConnectorObject, DeleteUserProvisioningConnectorResponse> {
    private static final Log log = LogFactory.getLog(DeleteUserCommandExecutor.class);

    @Override
    public DeleteUserProvisioningConnectorResponse perform(UserConnectorObject connectorObject) throws ConnectorException {
        DeleteUserProvisioningConnectorResponse response = new DeleteUserProvisioningConnectorResponse();
        response.setStatus(ResponseStatus.FAILURE);
        try {
            String id = existInFreshservice(connectorObject, FreshserviceAPI.LIST_REQUESTER_USERS);
            if (id != null) {
                exchange(FreshserviceAPI.DELETE_USER, connectorObject, null, new String[]{id}, null);
            } else {
                id = existInFreshservice(connectorObject, FreshserviceAPI.LIST_AGENT_USERS);
                if (id != null) {
                    exchange(FreshserviceAPI.DELETE_AGENT, connectorObject, null, new String[]{id}, null);
                }
            }

            response.setStatus(ResponseStatus.SUCCESS);
        } catch (Exception e) {
            log.error(e);
            response.setErrorCode(ResponseCode.FAIL_CONNECTION);
            response.setErrorText(e.getMessage());
        }
        return response;
    }
}
