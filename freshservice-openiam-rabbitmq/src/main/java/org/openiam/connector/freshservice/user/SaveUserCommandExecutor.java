package org.openiam.connector.freshservice.user;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openiam.api.connector.model.UserConnectorObject;
import org.openiam.api.connector.user.response.SaveUserProvisioningConnectorResponse;
import org.openiam.base.ws.ResponseCode;
import org.openiam.base.ws.ResponseStatus;
import org.openiam.common.beans.jackson.CustomJacksonMapper;
import org.openiam.connector.core.base.exception.ConnectorErrorCode;
import org.openiam.connector.core.base.exception.ConnectorException;
import org.openiam.connector.freshservice.base.AbstractFreshserviceConnectorCommand;
import org.openiam.connector.freshservice.base.FreshserviceAPI;
import org.openiam.connector.freshservice.model.AgentFreshserviceResponse;
import org.openiam.connector.freshservice.model.AgentUpdate;
import org.openiam.connector.freshservice.model.FreshServiceRequestResponseWrapper;
import org.openiam.connector.freshservice.model.FreshserviceUser;
import org.springframework.stereotype.Component;

@Component("saveUserCommand")
public class SaveUserCommandExecutor extends AbstractFreshserviceConnectorCommand<UserConnectorObject, SaveUserProvisioningConnectorResponse> {

    private static final Log log = LogFactory.getLog(SaveUserCommandExecutor.class);


    @Override
    public SaveUserProvisioningConnectorResponse perform(UserConnectorObject userConnectorObject) throws ConnectorException {
        SaveUserProvisioningConnectorResponse response = new SaveUserProvisioningConnectorResponse();
        response.setStatus(ResponseStatus.FAILURE);
        try {
            String id = existInFreshservice(userConnectorObject, FreshserviceAPI.LIST_REQUESTER_USERS);
            if (id != null) {
                doUpdate(userConnectorObject, id);
            } else {
                id = existInFreshservice(userConnectorObject, FreshserviceAPI.LIST_AGENT_USERS);
                if (id != null) {
                    doAgentUpdate(userConnectorObject, id);
                } else {
                    doAdd(userConnectorObject);
                }
            }
            response.setStatus(ResponseStatus.SUCCESS);
        } catch (Exception e) {
            log.error(e);
            response.setErrorCode(ResponseCode.FAIL_CONNECTION);
            response.setErrorText(e.getMessage());
        }
        return response;
    }

    private void doAdd(UserConnectorObject userConnectorObject) throws ConnectorException {
        FreshserviceUser user = extensibleUserToFreshservice(userConnectorObject);
        FreshServiceRequestResponseWrapper wrapper = new FreshServiceRequestResponseWrapper();
        wrapper.setUser(user);
        try {
            exchange(FreshserviceAPI.CREATE_USER,
                    userConnectorObject, null, null, wrapper
            );
        } catch (Exception e) {
            throw new ConnectorException(ConnectorErrorCode.SAVE_OPERATION_ERROR, e);
        }
    }

    private void doAgentUpdate(UserConnectorObject userConnectorObject, String id) throws ConnectorException {
        try {
            AgentFreshserviceResponse agentResponse = exchange(FreshserviceAPI.GET_AGENT_BY_ID, userConnectorObject,
                    null,
                    new String[]{id},
                    null,
                    AgentFreshserviceResponse.class);
            if (agentResponse != null && agentResponse.getAgent() != null) {
                AgentUpdate updateRequest = new AgentUpdate();
                merge(userConnectorObject, updateRequest);
                if (log.isDebugEnabled()) {
                    CustomJacksonMapper mapper = new CustomJacksonMapper();
                    log.debug(mapper.writeValueAsString(updateRequest));
                }
                exchange(FreshserviceAPI.UPDATE_AGENT,
                        userConnectorObject, null, new String[]{id}, updateRequest,
                        AgentUpdate.class
                );
            }
        } catch (Exception e) {
            throw new ConnectorException(ConnectorErrorCode.SAVE_OPERATION_ERROR, e);
        }
    }

    private void doUpdate(UserConnectorObject userConnectorObject, String id) throws ConnectorException {
        try {
            FreshserviceUser user = exchange(FreshserviceAPI.GET_USER_BY_ID, userConnectorObject, null, new String[]{id}, null).getUser();
            FreshServiceRequestResponseWrapper wrapper = new FreshServiceRequestResponseWrapper();
            merge(userConnectorObject, user);
            wrapper.setUser(user);
            exchange(FreshserviceAPI.UPDATE_USER,
                    userConnectorObject, null, new String[]{id}, wrapper
            );
        } catch (Exception e) {
            throw new ConnectorException(ConnectorErrorCode.SAVE_OPERATION_ERROR, e);
        }
    }
}
