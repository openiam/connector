package org.openiam.connector.freshservice.user;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openiam.api.connector.model.UserConnectorObject;
import org.openiam.api.connector.user.response.TestProvisioningConnectorResponse;
import org.openiam.base.ws.ResponseStatus;
import org.openiam.connector.core.base.exception.ConnectorException;
import org.openiam.connector.freshservice.base.AbstractFreshserviceConnectorCommand;
import org.openiam.connector.freshservice.base.FreshserviceAPI;
import org.openiam.connector.freshservice.model.FreshServiceRequestResponseWrapper;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component("testConnectionCommand")
public class TestConnectionCommandExecutor extends AbstractFreshserviceConnectorCommand<UserConnectorObject, TestProvisioningConnectorResponse> {
    private static final Log log = LogFactory.getLog(TestConnectionCommandExecutor.class);

    @Override
    public TestProvisioningConnectorResponse perform(UserConnectorObject connectorObject) throws ConnectorException {
        TestProvisioningConnectorResponse response = new TestProvisioningConnectorResponse();
        response.setStatus(ResponseStatus.FAILURE);
        try {
            Map<String, String> queryParams = new HashMap<>();
            queryParams.put("state"
                    , "unverified");
            List<FreshServiceRequestResponseWrapper> data = exchangeList(FreshserviceAPI.LIST_REQUESTER_USERS, connectorObject, queryParams, null, null);
            if (log.isInfoEnabled()) {
                log.info("Got data for test connection. unverified users number is " + (data == null ? 0 : data.size()));
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Authentication failed: " + e);
        }
        return response;
    }
}
