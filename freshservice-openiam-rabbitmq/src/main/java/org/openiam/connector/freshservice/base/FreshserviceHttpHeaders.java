package org.openiam.connector.freshservice.base;

import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.nio.charset.Charset;

public class FreshserviceHttpHeaders extends HttpHeaders {
    private static final long serialVersionUID = 2394538554436196720L;

    public FreshserviceHttpHeaders(String username, String password) {
        String auth = username + ":" + password;
        byte[] encodedAuth = Base64.encodeBase64(
                auth.getBytes(Charset.forName("US-ASCII")));
        String authHeader = "Basic " + new String(encodedAuth);
        set("Authorization", authHeader);
        setContentType(MediaType.APPLICATION_JSON);
    }
}
